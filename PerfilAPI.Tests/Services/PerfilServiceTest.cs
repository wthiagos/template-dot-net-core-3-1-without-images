using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using PerfilAPI.Entities;
using PerfilAPI.Helpers;
using PerfilAPI.Services;
using Xunit;

namespace PerfilAPI.Tests.Services
{
    public class PerfilServiceTest : IDisposable
    {
        private readonly PerfilService PerfilService;
        private readonly DbContextOptions<DataContext> Options;
        private readonly SqliteConnection Connection;
        private readonly List<Perfil> Diferenciais = new List<Perfil>
        {
            new Perfil {
                Id = 1,
                Descricao = "Mais barato"
            },
            new Perfil {
                Id = 2,
                Descricao = "Atende em mais hospitais"
            },
        };

        public PerfilServiceTest()
        {
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();

            Options = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(Connection)
                    .Options;

            using (DataContext context = new DataContext(Options))
                context.Database.EnsureCreated();

            PerfilService = new PerfilService(new DataContext(Options));
        }

        [Fact]
        public async Task ListaSucesso()
        {
            await PerfilService.Criar(Diferenciais);

            var result = await PerfilService.Listar();
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task BuscarErro()
        {
            await PerfilService.Criar(Diferenciais);

            await Assert.ThrowsAsync<AppException>(() => PerfilService.Buscar(999));
        }

        [Fact]
        public async Task BuscarErroZero()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Buscar(0));
        }

        [Fact]
        public async Task BuscarSucesso()
        {
            await PerfilService.Criar(Diferenciais);

            var result = await PerfilService.Buscar(1);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task CriarSucesso()
        {
            await PerfilService.Criar(Diferenciais);
            return;
        }

        [Fact]
        public async Task CriarErroNulo()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Criar(null));
        }

        [Fact]
        public async Task CriarErroVazio()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Criar(new List<Perfil>()));
        }

        [Fact]
        public async Task AtualizarSucesso()
        {
            await PerfilService.Criar(Diferenciais);
            await PerfilService.Atualizar(Diferenciais);
            return;
        }

        [Fact]
        public async Task AtualizarErroNulo()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Atualizar(null));
        }

        [Fact]
        public async Task AtualizarErroVazio()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Atualizar(new List<Perfil>()));
        }

        [Fact]
        public async Task ExcluirSucesso()
        {
            await PerfilService.Criar(Diferenciais);
            await PerfilService.Excluir(1);
            return;
        }

        [Fact]
        public async Task ExcluirErro()
        {
            await PerfilService.Criar(Diferenciais);
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Excluir(999));
        }

        [Fact]
        public async Task ExcluirErroZero()
        {
            await Assert.ThrowsAsync<AppException>(() => PerfilService.Excluir(0));
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
