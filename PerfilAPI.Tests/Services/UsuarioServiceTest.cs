using System;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using PerfilAPI.Entities;
using PerfilAPI.Helpers;
using PerfilAPI.Services;
using Xunit;

namespace PerfilAPI.Tests
{
    public class UsuarioServiceTest : IDisposable
    {
        private readonly UsuarioService UsuarioService;
        private readonly DbContextOptions<DataContext> Options;
        private readonly SqliteConnection Connection;
        private readonly Usuario Usuario = new Usuario
        {
            DataCadastro = DateTime.Now,
            Email = "thiago@email.com",
            Id = 1,
            Nome = "Thiago",
        };

        public UsuarioServiceTest()
        {
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();

            Options = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(Connection)
                    .Options;

            using (DataContext context = new DataContext(Options))
                context.Database.EnsureCreated();

            UsuarioService = new UsuarioService(new DataContext(Options));
        }

        [Fact]
        public void ValidarSucesso()
        {
            bool result = UsuarioService.Validar(1);

            Assert.False(result);
        }

        [Fact]
        public void ValidarErro()
        {
            Assert.Throws<AppException>(() => UsuarioService.Validar(0));
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
