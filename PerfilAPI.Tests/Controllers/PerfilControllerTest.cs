using Xunit;
using System;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Options;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using PerfilAPI.Helpers;
using PerfilAPI.Entities;
using PerfilAPI.Models.Perfil;
using PerfilAPI.Services;
using PerfilAPI.Controllers;

namespace PerfilAPI.Tests.Controllers
{
    public class PerfilControllerTest : IDisposable
    {
        private readonly PerfilService PerfilService;
        private readonly IMapper Mapper;
        private readonly IOptions<AppSettings> AppSettings;
        private readonly DbContextOptions<DataContext> DbOptions;
        private readonly SqliteConnection Connection;
        private readonly IConfiguration Configuration;
        private readonly List<Perfil> Diferenciais = new List<Perfil>
        {
            new Perfil {
                Id = 1,
                Descricao = "Mais barato"
            },
            new Perfil {
                Id = 2,
                Descricao = "Atende em mais hospitais"
            }
        };

        private readonly List<PerfilCreateModel> DiferenciaisCreateModel = new List<PerfilCreateModel>
        {
            new PerfilCreateModel {
                Descricao = "Mais barato"
            },
            new PerfilCreateModel {
                Descricao = "Atende em mais hospitais"
            },
        };

        private readonly List<PerfilUpdateModel> DiferenciaisUpdateModel = new List<PerfilUpdateModel>
        {
            new PerfilUpdateModel {
                Id = 1,
                Descricao = "Mais barato"
            },
            new PerfilUpdateModel {
                Id = 2,
                Descricao = "Atende em mais hospitais"
            },
        };

        public PerfilControllerTest()
        {
            AutoMapperProfile mapperProfile = new AutoMapperProfile();
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();

            DbOptions = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(Connection)
                    .Options;

            using (DataContext context = new DataContext(DbOptions))
                context.Database.EnsureCreated();

            PerfilService = new PerfilService(new DataContext(DbOptions));
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            IConfigurationSection appSettingsSection = Configuration.GetSection("AppSettings");

            var appSettings = appSettingsSection.Get<AppSettings>();

            AppSettings = Options.Create<AppSettings>(appSettings);

            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mapperProfile);
            });

            Mapper = config.CreateMapper();
        }

        [Fact]
        public void Construtor()
        {
            var result = new PerfilController(PerfilService, Mapper, AppSettings);
            Assert.NotNull(result);
        }

        [Fact]
        public async void GetSucesso()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            var result = await controller.Get();
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void GetByIdSucesso()
        {
            await PerfilService.Criar(Diferenciais);
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            IActionResult result = await controller.GetById(1);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void GetByIdErro()
        {
            await PerfilService.Criar(Diferenciais);
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.GetById(999));
        }

        [Fact]
        public async void GetByIdErroZero()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.GetById(0));
        }

        [Fact]
        public async void PostSucesso()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            IActionResult result = await controller.Post(DiferenciaisCreateModel);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void PostErroNulo()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Post(null));
        }

        [Fact]
        public async void PostErroVazio()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Post(new List<PerfilCreateModel>()));
        }

        [Fact]
        public async void PutSucesso()
        {
            await PerfilService.Criar(Diferenciais);

            using (DataContext context = new DataContext(DbOptions))
            {
                PerfilService service = new PerfilService(context);
                PerfilController controller = new PerfilController(service, Mapper, AppSettings);
                IActionResult result = await controller.Put(DiferenciaisUpdateModel);
                Assert.IsType<OkResult>(result);
            }
        }

        [Fact]
        public async void PutErroNulo()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Put(null));
        }

        [Fact]
        public async void PutErroVazio()
        {
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Put(new List<PerfilUpdateModel>()));
        }

        [Fact]
        public async void DeleteSucesso()
        {
            await PerfilService.Criar(Diferenciais);
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            IActionResult result = await controller.Delete(1);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void DeleteErro()
        {
            await PerfilService.Criar(Diferenciais);
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Delete(999));
        }

        [Fact]
        public async void DeleteErroZero()
        {
            await PerfilService.Criar(Diferenciais);
            PerfilController controller = new PerfilController(PerfilService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Delete(0));
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
