//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================

using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using PerfilAPI.Entities;
using PerfilAPI.Models.Perfil;

namespace PerfilAPI.Helpers
{
    [ExcludeFromCodeCoverage]
    public class AutoMapperProfile : Profile
    {
        ///<summary>
        ///
        ///Esse método serve para mapear os objetos de entidades com suas models
        ///
        ///</summary>
        public AutoMapperProfile()
        {
            CreateMap<PerfilUpdateModel, Perfil>();
            CreateMap<PerfilCreateModel, Perfil>();
            CreateMap<Perfil, PerfilModel>();
        }
    }
}