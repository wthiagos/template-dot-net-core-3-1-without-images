//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace PerfilAPI.Helpers
{
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class AppException : Exception
    {
        public AppException()
        {
        }

        public AppException(string message)
            : base(message)
        {
        }

        public AppException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected AppException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}