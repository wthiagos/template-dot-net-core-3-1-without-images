//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PerfilAPI.Entities;
using PerfilAPI.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace PerfilAPI.Services
{
    public interface IPerfilService
    {
        Task<List<Perfil>> Listar();
        Task<Perfil> Buscar(int id);
        Task Criar(List<Perfil> perfis);
        Task Atualizar(List<Perfil> perfis);
        Task Excluir(int id);
    }

    public class PerfilService : IPerfilService
    {
        private readonly DataContext Db;

        ///<summary>
        ///
        ///Esse método serve para atribuir os objetos recebidos para injeção de depencia
        ///para os atributos da classe
        ///
        ///</summary>
        ///<param name="db">Contexto do banco de dados</param>
        public PerfilService(DataContext db)
        {
            Db = db;
        }

        ///<summary>
        ///
        ///Esse método serve para listar todos os perfis da base.
        ///
        ///</summary>
        public async Task<List<Perfil>> Listar()
        {
            return await Db.Set<Perfil>().ToListAsync();
        }

        ///<summary>
        ///
        ///Esse método serve para buscar um perfil por Id.
        ///
        ///</summary>
        ///<param name="id">Id do perfil</param>
        public async Task<Perfil> Buscar(int id)
        {
            if (id == 0)
                throw new AppException("O id do perfil não pode ser igual a 0");

            Perfil tag = await Db.Set<Perfil>().FindAsync(id);

            if (tag == null)
                throw new AppException("Perfil não encontrado");

            return tag;
        }

        ///<summary>
        ///
        ///Esse método serve inserir perfis na base.
        ///
        ///</summary>
        ///<param name="perfis">Model de perfis para ser inserido</param>
        public async Task Criar(List<Perfil> perfis)
        {
            if (perfis == null || !perfis.Any())
                throw new AppException("Deve-se ter ao menos um perfil");

            await Db.Set<Perfil>().AddRangeAsync(perfis);
            await Db.SaveChangesAsync();
        }

        ///<summary>
        ///
        ///Esse método serve atualizar diferencias na base.
        ///
        ///</summary>
        ///<param name="perfis">Model de diferencias para ser atualizado</param>
        public async Task Atualizar(List<Perfil> perfis)
        {
            if (perfis == null || !perfis.Any())
                throw new AppException("Deve-se ter ao menos um perfil");

            Db.Set<Perfil>().UpdateRange(perfis);

            await Db.SaveChangesAsync();
        }

        ///<summary>
        ///
        ///Esse método serve excluir um perfil na base.
        ///
        ///</summary>
        ///<param name="id">Id do perfil a ser excluído</param>
        public async Task Excluir(int id)
        {
            if (id == 0)
                throw new AppException("O id do perfil não pode ser igual a 0");

            Perfil entity = await Db.Set<Perfil>().FindAsync(id);

            if (entity != null)
            {
                Db.Set<Perfil>().Remove(entity);

                await Db.SaveChangesAsync();
            }
            else
            {
                throw new AppException("Perfil não encontrado");
            }
        }
    }
}