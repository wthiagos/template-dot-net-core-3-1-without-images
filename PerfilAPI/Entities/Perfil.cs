//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace PerfilAPI.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("Perfil")]
    public class Perfil
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("Descricao")]
        public string Descricao { get; set; }
        [Column("DataCadastro")]
        public DateTime DataCadastro { get; set; }
    }
}