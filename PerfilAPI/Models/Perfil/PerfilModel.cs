//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================

using System;
using System.Diagnostics.CodeAnalysis;

namespace PerfilAPI.Models.Perfil
{
    [ExcludeFromCodeCoverage]
    public class PerfilModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}