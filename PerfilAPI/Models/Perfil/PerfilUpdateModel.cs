//===============================================================================
//Web API Perfil
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Perfil 
//==============================================================================

using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace PerfilAPI.Models.Perfil
{
    [ExcludeFromCodeCoverage]
    public class PerfilUpdateModel
    {
        [Required]
        public int? Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Descricao { get; set; }
    }
}